
#include "main.h"

SOCKADDR_IN SockAddr;
SOCKET Socket;
j3f_expecting expecting = J3F_CATEGORIES;

int main(int argc, char *argv[])
{
	WSADATA wsaData;
	char *specifier;
	j3f_files fileKind;

	printf("SpioR's JK2Files/JK3Files downloading bot\n");
	printf("Original version by SpioR, with additional revisions by eezstreet\n");
	printf("Copyright (c) 2013\n");

	// eezstreet edit: let's do stuff with the args, specify whether we're going for JK2 or JKA, shall we?
	if(argc != 2)
	{
printUsageMessage:
		printf("Usage: jk3files.exe \"<JK2/JKA>\"\n");
		return 0;
	}

	specifier = argv[1];
	if(!specifier || !specifier[0])
	{
		printf("Something went terribly wrong. Check and make sure you're entering \"JK2\" or \"JKA\" correctly as the arg.\n");
		return 0;
	}

	// I'm going to be an evil fucker and force them to wait ---eez
	Sleep(2000);

	if((fileKind = J3F_DetermineFileTypeFromArgument(specifier)) == J3FT_UNKNOWN)
	{
		goto printUsageMessage;	// too lazy to write this into a proper function --eez
	}

	printf("Retrieving host...");

	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		printf("WSAStartup failed!\n");
		return FALSE;
	}

	struct hostent* pHE = gethostbyname("jediknight3.filefront.com");
	if(!pHE)
	{
		printf("\njk3files is down. IT'S TOO LATE!!!\n");
		system("pause");
		return FALSE;
	}
	u_long addr = *((u_long*)pHE->h_addr_list[0]);
	if(!addr)
	{
		printf("\ngethostbyname failed!\n");
		return FALSE;
	}
	printf("ok\n");

	Sleep(700);	// aesthetics

	printf("Connecting to host...");

	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(80);
	SockAddr.sin_addr.s_addr = addr;

	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(connect(Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr)) == INVALID_SOCKET)
	{
		printf("\nconnection failed!\n");
		return FALSE;
	}
	printf("success!\n");

	char urls[MAX_URLS][MAX_URL_LEN] = { 0 };
	int num_url = 0;
	int curr_url = 0;
	file_t *files= NULL;
	int num_file = 0;
	int curr_file = 0;

	int num_page = 0;

	int num_files = 0; long num_dl = 0; double num_GB = 0;
	while(1)
	{
		if(expecting == J3F_CATEGORIES)
		{
			switch(fileKind)
			{
				default:	// really shouldn't happen but whatever
				case J3FT_JKA:
					J3F_GetData(
						"/files/Jedi_Knight_III;38index", 
						urls, &num_url, 
						NULL, &num_file, 
						&num_page, NULL, 
						&num_files, &num_dl, &num_GB
						);
					break;
					
				case J3FT_JK2:
					J3F_GetData(
						"/files/Jedi_Knight_II;7index", 
						urls, &num_url, 
						NULL, &num_file, 
						&num_page, NULL, 
						&num_files, &num_dl, &num_GB
						);
					break;

			}

			if(!files)
			{
				files = (file_t*)malloc(num_files*sizeof(file_t));
				memset(files, 0, num_files*sizeof(file_t));
			}

			if(num_page == 0)
				expecting = J3F_FILES;
		}
		else if(expecting == J3F_FILES)
		{
			FILE *file = fopen("files.txt", "r");
			if(file && num_files)
			{
				int n_files = 0;
				char line[MAX_FILE_LEN] = { 0 };
				while(fgets(line, sizeof(line), file))
					n_files++;
				fclose(file);
				if(n_files == num_files-1)
				{
					// Reload the file list if the number of files is the same
					file = fopen("files.txt", "r");
					while(fgets(line, sizeof(line), file))
					{
						strncpy(files[num_file].dir, line, strchr(line, ';')-line);
						strncpy(files[num_file].file, line+(strchr(line, ';')-line)+1, MAX_FILE_LEN);
						files[num_file].file[strlen(files[num_file].file)-1] = 0;
						num_file++;
					}
					fclose(file);
				}
				else
				{
					// Empty the file if the number of files has changed
					file = fopen("files.txt", "w");
					fclose(file);
				}
			}

			if(num_file == num_files-1)
			{
				expecting = J3F_FILEPAGE;
			}
			else
			{
				J3F_GetData(urls[curr_url], 
					urls, &num_url, 
					files, &num_file, 
					&num_page, &curr_url, 
					NULL, NULL, NULL
					);
				system("cls");
				printf("Getting file list... %04i/%i\n", num_file, num_files-1);
			}
		}
		else if(expecting == J3F_FILEPAGE)
		{
			if(curr_file == num_files-1)
				break;
			char title[64] = { 0 };
			sprintf(title, "%04i/%i", curr_file, num_files-1);
			SetConsoleTitle(title);
			J3F_DownloadFile(files[curr_file++]);
		}
	}
	free(files);
	printf("Done!\n");
	system("pause");
	return TRUE;
}